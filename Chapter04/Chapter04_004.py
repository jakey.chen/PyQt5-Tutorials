#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
事件发送
'''

import sys
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QApplication,
                             QPushButton,
                             QMainWindow)


class Example(QMainWindow):
    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        btn1 = QPushButton("Button 1", self)
        btn2 = QPushButton("Button 2", self)

        btn1.move(30, 50)
        btn2.move(150, 50)

        btn1.clicked.connect(self.btnClicked)
        btn2.clicked.connect(self.btnClicked)

        self.statusBar()
        
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle("Event sender")
        self.show()

    def btnClicked(self):
        sender = self.sender()
        self.statusBar().showMessage(sender.text() + ' was pressed')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
