#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
下拉选框
'''

import sys
from PyQt5.QtWidgets import (QWidget,
                             QLabel,
                             QComboBox,
                             QApplication)


class Example(QWidget):
    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        self.lbl = QLabel("Ubuntu", self)

        combobox = QComboBox(self)
        combobox.addItem("Ubuntu")
        combobox.addItem("Mandriva")
        combobox.addItem("Fedora")
        combobox.addItem("Arch")
        combobox.addItem("Gentoo")

        combobox.move(50, 50)
        self.lbl.move(50, 150)

        combobox.activated[str].connect(self.onActivated)

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle("QComboBox")
        self.show()

    def onActivated(self, text):
        self.lbl.setText(text)
        self.lbl.adjustSize()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
