#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
工具栏
'''

import sys
from PyQt5.QtWidgets import (QApplication,
                             qApp,
                             QAction,
                             QMainWindow)
from PyQt5.QtGui import QIcon


class Example(QMainWindow):
    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        exitAct = QAction(QIcon("snake.ico"), "Exit", self)
        exitAct.setShortcut("Ctrl+Q")
        exitAct.triggered.connect(qApp.quit)

        self.toolbar = self.addToolBar("Exit")
        self.toolbar.addAction(exitAct)

        self.setGeometry(300, 300, 300, 250)
        self.setWindowTitle("Toolbar")
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
