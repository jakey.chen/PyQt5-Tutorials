#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
子菜单
'''

import sys
from PyQt5.QtWidgets import (QApplication,
                             QAction,
                             QMenu,
                             QMainWindow)


class Example(QMainWindow):
    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        menubar = self.menuBar()
        fileMenu = menubar.addMenu("File")

        impMenu = QMenu("Import", self)
        impAct = QAction("Import Mail", self)
        impMenu.addAction(impAct)

        expMenu = QMenu("Export", self)
        expAct = QAction("Export Mail", self)
        expMenu.addAction(expAct)

        newAct = QAction("New", self)

        fileMenu.addAction(newAct)
        fileMenu.addMenu(impMenu)
        fileMenu.addMenu(expMenu)

        self.setGeometry(300, 300, 300, 250)
        self.setWindowTitle("SubMenu")
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
