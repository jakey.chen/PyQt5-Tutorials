#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
勾选菜单
'''

import sys
from PyQt5.QtWidgets import (QApplication,
                             QAction,
                             QMainWindow)


class Example(QMainWindow):
    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        self.statusbar = self.statusBar()
        self.statusbar.showMessage("Ready")

        menubar = self.menuBar()
        viewMenu = menubar.addMenu("View")

        viewStatAct = QAction("View statusbar", self, checkable=True)
        viewStatAct.setStatusTip("View statusbar")
        viewStatAct.setChecked(True)
        viewStatAct.triggered.connect(self.toggleMenu)

        viewMenu.addAction(viewStatAct)

        self.setGeometry(300, 300, 300, 250)
        self.setWindowTitle("Check Menu")
        self.show()

    def toggleMenu(self, state):
        if state:
            self.statusbar.show()
        else:
            self.statusbar.hide()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
