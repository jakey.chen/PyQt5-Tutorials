#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
主窗口
'''

import sys
from PyQt5.QtWidgets import (QMainWindow,
                             QTextEdit,
                             QAction,
                             QApplication)
from PyQt5.QtGui import QIcon


class Example(QMainWindow):
    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        textEdit = QTextEdit()
        self.setCentralWidget(textEdit)

        exitAct = QAction(QIcon("snake.ico"), "Exit", self)
        exitAct.setShortcut("Ctrl+Q")
        exitAct.setStatusTip("Exit Application")
        exitAct.triggered.connect(self.close)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu("&File")
        fileMenu.addAction(exitAct)

        self.toolbar = self.addToolBar("Exit")
        self.toolbar.addAction(exitAct)

        self.statusBar()

        self.setGeometry(300, 300, 300, 250)
        self.setWindowTitle("Toolbar")
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
