#!/usr/bin/python3
# _*_ coding: utf-8 _*_

'''
窗口居中
'''

import sys
from PyQt5.QtWidgets import (QWidget,
                             QDesktopWidget,
                             QApplication)


class Example(QWidget):
    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        self.resize(280, 160)
        self.center()

        self.setWindowTitle("Center")
        self.show()

    def center(self):
        qr = self.frameGeometry()
        # print(qr)
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
