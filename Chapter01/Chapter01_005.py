#!/usr/bin/python3
# _*_ coding: utf-8 _*_

'''
消息盒子
'''

import sys
from PyQt5.QtWidgets import (QWidget,
                             QMessageBox,
                             QApplication)


class Example(QWidget):
    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle("Message Box")
        self.show()

    def closeEvent(self, event):
        reply = QMessageBox.question(self, "Message",
                                     "Are you sure to quit?", QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
